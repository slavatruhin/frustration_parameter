import os
import numba
import numpy
import matplotlib.pyplot as plt
import configparser
import time

start = time.time()

conf = configparser.ConfigParser()
conf.read("data/conf.ini")
dos_dir = "data/dos" + conf["Sample"]["spins"] + "/"
stat_dir = "data/statsum" + conf["Sample"]["spins"] + "/"
fig_dir = "data/figures" + conf["Sample"]["spins"] + "/"

T = numpy.linspace(float(conf["Temperature"]["start"]),
                   float(conf["Temperature"]["stop"]),
                   int(conf["Temperature"]["steps"]))
H = numpy.linspace(float(conf["Field"]["start"]),
                   float(conf["Field"]["stop"]),
                   int(conf["Field"]["steps"]))


@numba.njit(parallel=True)
def frustration_parameter(g, e_total, mass_t, field, n):
    frus_param = numpy.zeros(mass_t.size)
    for t in numba.prange(mass_t.size):
        frus_param[t] = calc_fp(g, e_total, mass_t[t], field, n)
    return frus_param


@numba.njit
def calc_fp(g, e_total, t, field, n):
    stat = g * numpy.exp(-e_total / t)
    z = stat.sum()
    p = stat / z
    e_aver = (e_total * p).sum()
    e_max = 2 * (n - 1) * n + n ** 2 * field
    return (e_max + e_aver) / (2 * e_max)


file_count = 0
if conf["Recalculate"]["recalculate"] == "True":
    for h in H:
        P_plus = numpy.array([])
        fp = numpy.zeros(shape=(0, int(conf["Temperature"]["steps"])))
        for file in os.listdir(dos_dir):
            file = dos_dir + file
            gem = numpy.loadtxt(open(file), skiprows=4).T
            N = int(open(file).readlines()[0].rstrip())
            J_sum = int(open(file).readlines()[2].rstrip())
            P_plus = numpy.append(P_plus, (2 * N * (N - 1) + J_sum) / (4 * N * (N - 1)))
            G = gem[0]
            E = gem[1]
            M = gem[2]
            E_total = E - M * h
            E_total_plus = E + M * h
            # E_total += abs(numpy.min(E_total))
            fp = numpy.append(fp, [frustration_parameter(G, E_total, T, h, N)], axis=0)
        numpy.save(stat_dir + "P_+_h" + str(h), P_plus)
        numpy.savetxt(stat_dir + "P_+_h" + str(h) + "txt", P_plus)
        numpy.save(stat_dir + "fp_h" + str(h), fp)
        numpy.savetxt(stat_dir + "fp_h" + str(h) + ".txt", fp)
label = []
plt.figure(dpi=300)
# plt.grid(True)
plt.xlim([0, 1])
# plt.ylim(bottom=0)

h = int(conf["Plot"]["field"])
print("h = ", H[h])
ls = list()
t_label = 0
marklist = ['+', '*', 'o']
for temp in range(len(T)):
    for file in os.listdir(dos_dir):
        P_plus_loaded = numpy.load(stat_dir + "P_+_h" + str(H[h]) + ".npy")
        fp_loaded = numpy.load(stat_dir + "fp_h" + str(H[h]) + ".npy")
        idx = P_plus_loaded.argsort()
        P_plus_loaded = P_plus_loaded[idx]
        fp_loaded = fp_loaded[idx]
    fp_only = fp_loaded[:, temp]
    if conf["Plot"]["plot"] == "Frustration_parameter" and (temp == 2 or temp == 4 or temp == 6):
        plt.scatter(P_plus_loaded, fp_only, s=5, color='black', marker=marklist[t_label])
        plt.xlabel("$P_{+}$", fontsize=18)
        plt.ylabel("$Fp$", fontsize=18)
        label.append(f"T = {int(T[temp] * 10) / 10}")
        plt.legend(label, fontsize=18)
        plt.tick_params(axis='both', which='major', labelsize=12)
        t_label += 1
    if conf["Plot"]["plot"] == "Phase_diagram":
        j = 0
        fp_only = fp_loaded[:, temp]
        Plateau_aver = numpy.average(fp_only[(P_plus_loaded > 0.3) & (P_plus_loaded < 0.7)])
        Plateau_min = numpy.min(fp_only[(P_plus_loaded > 0.3) & (P_plus_loaded < 0.7)])
        Plateau_max = numpy.max(fp_only[(P_plus_loaded > 0.3) & (P_plus_loaded < 0.7)])
        while fp_only[j] <= (Plateau_aver - (Plateau_aver - Plateau_min) * 1.2):
            j += 1
        k = fp_only.size - 1
        while fp_only[k] <= (Plateau_aver - (Plateau_aver - Plateau_min) * 1.2):
            k -= 1
        plt.scatter(P_plus_loaded[k], T[temp])
        plt.scatter(P_plus_loaded[j], T[temp])
        ls.append((P_plus_loaded[k], P_plus_loaded[j], T[temp]))
    if conf["Plot"]["plot"] == "Phase_diagram2":
        j = 0
        c1_left = numpy.array([])
        c2_left = numpy.array([])
        seed = 100
        P_plus_lsm = numpy.vstack([P_plus_loaded, numpy.ones(len(P_plus_loaded))]).T
        for i in range(int(len(P_plus_loaded) * 0.6)):
            left = int(len(P_plus_loaded) * 0.4) - seed + i
            right = int(len(P_plus_loaded) * 0.4) + i
            cc1, cc2 = numpy.linalg.lstsq(P_plus_lsm[left:right, :], fp_only[left:right], rcond=None)[0]
            c1_left = numpy.append(c1_left, cc1)
            c2_left = numpy.append(c2_left, cc2)
            # plt.plot(P_plus_loaded[left:right], P_plus_loaded[left:right] * cc1 + cc2)
        # numpy.savetxt(stat_dir + "squareC_T" + str(temp) + ".txt", c1)
        aver_c1 = numpy.average(c1_left[int(0.4 * len(P_plus_loaded)):int(0.5 * len(P_plus_loaded))])
        print(aver_c1)
        i = -1
        while c1_left[i] <= 1.6 * aver_c1:
            i -= 1
            if i < -len(c1_left):
                break
        plt.scatter(P_plus_loaded[i], T[temp], s=8, color='black')
    if conf["Plot"]["plot"] == "Phase_diagram3":
        carbone_AI = [[[0.135, 0.802],
                       [0.106, 0.817],
                       [0.126, 0.844],
                       [0.111, 0.871],
                       [0.108, 0.882],
                       [0.102, 0.923],
                       [0.066, 0.927],
                       [0.06, 0.945]],
                      [[0.13, 0.73],
                       [0.1, 0.731],
                       [0.082, 0.74],
                       [0.058, 0.77],
                       [0.052, 0.791],
                       [0.047, 0.89],
                       [0.05, 0.903],
                       [0.026, 0.941]]]
        plt.scatter(carbone_AI[0][temp][0], T[temp], s=18, color='black', marker='*')
        plt.scatter(carbone_AI[0][temp][1], T[temp], s=18, color='black', marker='*')
        t_max = numpy.load(stat_dir + "t_max_h" + str(H[h]) + ".npy")
        for i in range(len(P_plus_loaded)):
            if P_plus_loaded[i] > carbone_AI[0][3][0]:
                left = i
                break
        for i in range(len(P_plus_loaded)):
            if P_plus_loaded[i] > carbone_AI[0][5][1]:
                right = i
                break
        plt.scatter(P_plus_loaded[left:right], t_max[left:right], s=12, color='black', marker='+')
        plt.tick_params(axis='both', which='major', labelsize=14)
        plt.xlabel("$P_{+}$", fontsize=14)
        plt.ylabel("$kT / J$", fontsize=14)
    if conf["Plot"]["plot"] == "Phase_diagram4":
        maxP_len = numpy.array([])
        minP_len = numpy.array([])
        maxFp_len = numpy.array([])
        minFp_len = numpy.array([])
        c1_left = numpy.array([])
        c2_left = numpy.array([])
        c1_right = numpy.array([])
        c2_right = numpy.array([])
        samples = int(conf["Sample"]["samp_in_group"])
        seed = int(int(conf["Parameters"]["seed"]) / samples)
        for i in range(0, int(len(fp_only) / samples)):
            maxFP = fp_only[i * samples + 0]
            minFP = maxFP
            maxFP_idx = i * samples + 0
            minFP_idx = i * samples + 0
            for j in range(samples):
                if maxFP < fp_only[i * samples + j]:
                    maxFP = fp_only[i * samples + j]
                    maxFP_idx = i * samples + j
                if minFP > fp_only[i * samples + j]:
                    minFP = fp_only[i * samples + j]
                    minFP_idx = i * samples + j
            maxFp_len = numpy.append(maxFp_len, maxFP)
            minFp_len = numpy.append(minFp_len, minFP)
            maxP_len = numpy.append(maxP_len, P_plus_loaded[maxFP_idx])
            minP_len = numpy.append(minP_len, P_plus_loaded[minFP_idx])
        maxP_len_lsm = numpy.vstack([maxP_len, numpy.ones(len(maxP_len))]).T
        minP_len_lsm = numpy.vstack([minP_len, numpy.ones(len(minP_len))]).T
        c1_left, c2_left = numpy.linalg.lstsq(maxP_len_lsm[0:seed, :], maxFp_len[0:seed], rcond=None)[0]
        c1_right, c2_right = numpy.linalg.lstsq(maxP_len_lsm[-seed:-1, :], maxFp_len[-seed:-1], rcond=None)[0]
        maxDot_left = 0
        for j in range(len(maxFp_len)):
            iterableFP_len = maxFp_len[j] + 0.1 * maxFp_len[j]
            if iterableFP_len < (maxP_len[j] * c1_left + c2_left):
                maxDot_left = maxP_len[j]
                break
        for j in range(1, len(maxFp_len)):
            if (1.2 * minFp_len[-j]) < (minP_len[-j] * c1_right + c2_right):
                minDot_left = minP_len[-j]
                break
        plt.scatter(maxDot_left, T[temp], s=18, color='black', marker='*')
        plt.scatter(minDot_left, T[temp], s=18, color='black', marker='*')

        # plt.plot(maxP_len[:int(len(maxP_len)/6)], maxP_len[:int(len(maxP_len)/6)] * c1_left + c2_left)
        # plt.plot(maxP_len[-int(len(maxP_len)/6):-1], maxP_len[-int(len(maxP_len)/6):-1] * c1_right + c2_right)
        # plt.scatter(P_plus_loaded, fp_only, s=5, color='black')
        # plt.xlabel("$P_{+}$", fontsize=18)
        # plt.ylabel("$Fp$", fontsize=18)
        # plt.tick_params(axis='both', which='major', labelsize=12)
end = time.time() - start
print(end)
# plt.savefig(fig_dir + conf["Plot"]["plot"] + "_h" + str(int(H[h] * 10) * 0.1) + ".eps", format='eps', dpi=1000)
plt.show()
